#!/usr/bin/python
import pytest
from src.artificialmap.model import Model, ModelController
import numpy as np


# not a perfect hill
def hill(i, j):
    return -1 * (i + j) + 10


def building(i, j):
    return 10 * np.ones_like(i)


def hole(i, j):
    return i + j - 10


@pytest.fixture()
def cases():
    """ Model object; 3x3 matrix after resolving; size of generated model matrix; resolution"""
    return ((Model(model=hill, central_position=(0, 0), under_ground=False), np.array([[10, 9, 8], [9, 8, 7], [8, 7, 6]]), (3, 3), 1),
            (Model(model=hill, central_position=(0, 0), under_ground=True), np.array([[10, 9, 8], [9, 8, 7], [8, 7, 6]]), (3, 3), 1),
            (Model(model=hill, central_position=(1, 1), under_ground=False), np.array([[12, 11, 10], [11, 10, 9], [10, 9, 8]]), (3, 3), 1),
            (Model(model=hill, central_position=(1, 1), under_ground=True), np.array([[12, 11, 10], [11, 10, 9], [10, 9, 8]]), (3, 3), 1),
            (Model(model=building, central_position=(0, 0), under_ground=False), 10 * np.ones((3, 3)), (3, 3), 1),
            (Model(model=building, central_position=(0, 0), under_ground=True), 10 * np.ones((6, 6)), (3, 3), 2),
            (Model(model=building, central_position=(1, 1), under_ground=False), 10 * np.ones((3, 3)), (3, 3), 1),
            (Model(model=building, central_position=(1, 1), under_ground=True), 10 * np.ones((6, 6)), (3, 3), 2),
            (Model(model=hole, central_position=(0, 0), under_ground=False), np.zeros((6, 6)), (3, 3), 2),
            (Model(model=hole, central_position=(0, 0), under_ground=True), np.array([[-10, -9, -8], [-9, -8, -7], [-8, -7, -6]]), (3, 3), 1),
            (Model(model=hole, central_position=(1, 1), under_ground=False), np.zeros((6, 6)), (3, 3), 2),
            (Model(model=hole, central_position=(1, 1), under_ground=True), np.array([[-12, -11, -10], [-11, -10, -9], [-10, -9, -8]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=hole, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(0, 0), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(0, 0), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=False), np.array([[-12., -11., -10.], [-11., -10., -9.], [-10., -9., -8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=False), np.array([[-12., -11., -10.], [-11., -10., -9.], [-10., -9., -8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(1, 1), under_ground=True), additional_models=None, replace=True, bigger_values_only=True), np.array([[-12., -11., -10.], [-11., -10., -9.], [-10., -9., -8.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hole, central_position=(1, 1), under_ground=True), additional_models=None, replace=False, bigger_values_only=True), np.array([[-12., -11., -10.], [-11., -10., -9.], [-10., -9., -8.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[20., 18., 16.], [18., 16., 14.], [16., 14., 12.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[22., 20., 18.], [20., 18., 16.], [18., 16., 14.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[20., 19., 18.], [19., 18., 17.], [18., 17., 16.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[22., 21., 20.], [21., 20., 19.], [20., 19., 18.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=building, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[12., 11., 10.], [11., 10., 10.], [10., 10., 10.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[2., 2., 2.], [2., 2., 2.], [2., 2., 2.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=hill, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[12., 11., 10.], [11., 10., 9.], [10., 9., 8.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[0., 1., 2.], [1., 2., 3.], [2., 3., 4.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[0., 1., 2.], [1., 2., 3.], [2., 3., 4.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[-10., -9., -8.], [-9., -8., -7.], [-8., -7., -6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),

            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(0, 0), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=False), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=False), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=True, bigger_values_only=True), np.array([[10., 9., 8.], [9., 8., 7.], [8., 7., 6.]]), (3, 3), 1),
            (ModelController(main_model=Model(model=building, central_position=(1, 1), under_ground=True), additional_models=[Model(model=hole, central_position=(0, 0), under_ground=True), Model(model=hill, central_position=(0, 0), under_ground=True)], replace=False, bigger_values_only=True), np.array([[10., 10., 10.], [10., 10., 10.], [10., 10., 10.]]), (3, 3), 1)
            )


def test_resolve(cases):
    for case in cases:
        assert case[1][0, 0] == case[0].resolve(0, 0)
        assert case[1][1, 1] == case[0].resolve(1, 1)


def test_generate(cases):
    for i, case in enumerate(cases):
        matrix = case[0].generate(case[2], case[3])
        assert len(matrix) == case[2][0] * case[3] and len(matrix.T) == case[2][1] * case[3]
        assert np.array_equal(case[1], matrix)
