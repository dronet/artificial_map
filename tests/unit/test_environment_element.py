#!/usr/bin/python
import pytest
from src.artificialmap.elements import create_element
import numpy as np


class TestEnvironmentElement:
    @pytest.mark.parametrize(
        "relative_position, in_resolution, replace, upsample, first_element, second_element, output_element",
        [
            (
                    (2, 7), True, False, False,
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)),
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 7)),
                    create_element('building', length=10, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 5)).generate_element_from_model()),
            (
                    (1, 3), False, False, False,
                    create_element('building', length=2, width=2, height=15, size=(5, 5), resolution=2, central_position=(2, 2)),
                    create_element('building', length=2, width=2, height=15, size=(5, 5), resolution=2, central_position=(2, 7)),
                    create_element('building', length=10, width=2, height=15, size=(5, 5), resolution=2, central_position=(2, 7)).generate_element_from_model()),
            (
                    (2, 2), True, True, False,
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)),
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)),
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)).generate_element_from_model()),
            (
                    (2, 2), True, False, False,
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)),
                    create_element('building', length=4, width=4, height=15, size=(10, 10), resolution=1, central_position=(2, 2)),
                    create_element('building', length=4, width=4, height=30, size=(10, 10), resolution=1, central_position=(2, 2)).generate_element_from_model()),
            (
                    (3, 3), True, False, True,
                    create_element('building', length=2, width=2, height=15, size=(5, 5), resolution=3, central_position=(3, 3)),
                    create_element('building', length=2, width=2, height=15, size=(10, 10), resolution=1, central_position=(2, 7)),
                    create_element('building', length=2, width=2, height=30, size=(5, 5), resolution=3, central_position=(3, 3)).generate_element_from_model()),
            (
                    (3, 3), True, False, False,
                    create_element('building', length=2, width=2, height=15, size=(5, 5), resolution=3, central_position=(3, 3)),
                    create_element('building', length=2, width=2, height=15, size=(10, 10), resolution=1, central_position=(2, 7)),
                    create_element('building', length=2, width=2, height=30, size=(5, 5), resolution=1, central_position=(1, 1)).generate_element_from_model()),
        ]
    )
    def test_merge(self, first_element, second_element, relative_position, in_resolution, replace, upsample,
                   output_element):
        merged_by_method = (first_element.merge(other=second_element,
                                                relative_position=relative_position,
                                                in_resolution=in_resolution,
                                                replace=replace,
                                                upsample=upsample)).generate_element_from_model()

        assert output_element.size == merged_by_method.size
        assert output_element.resolution == merged_by_method.resolution
        assert np.array_equal(output_element.matrix, merged_by_method.matrix)
