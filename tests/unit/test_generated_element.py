import numpy as np
import pytest
import shutil
from src.artificialmap.generatedelement import GeneratedElement, redress_matrix
from _pytest import pathlib
from src.artificialmap.elements import create_element
from src.artificialmap.generatedelement import save, load


class TestGeneratedElement:
    @pytest.mark.parametrize('tested_matrix, resolution, threshold, expected_matrix',
                             [(np.array([[2, 1, 0, 0], [3, 7, 0, 0]]), 2, 0,
                               np.array([[2, 1], [3, 7]])),
                              (np.array([[0, 0, 2, 1], [0, 0, 3, 7]]), 2, 0,
                               np.array([[2, 1], [3, 7]])),
                              (np.array([[2, 1, 0, 0], [0, 0, 3, 7]]), 2, 0,
                               np.array([[2, 1, 0, 0], [0, 0, 3, 7]])),
                              (np.array([[2, 1, -1], [100, 0.0001, -9999], [-0.00001, -1, -10]]), 3, 0,
                               np.array([[0, 0, 0], [0, 2, 1], [0, 100, 0.333]])),
                              (np.array([[1, -1, 1, -1], [2, -2, 2, -2]]), 1, 0,
                               np.array([[1, -1, 1], [2, -2, 2]])),
                              (np.array([[1, 0, -1], [0, -1, -2], [-1, -2, -3]]), 3, -1,
                               np.array([[0, 0, 0], [0, 1, 0], [0, 0, -1]])),
                              (np.array([[13, 12, 14], [17, 11.99999, 12.0001], [20, 10, 20]]), 3, 12,
                               np.array([[13, 12, 14], [17, 12, 12.333], [20, 10, 20]])),
                              (np.array([[100, 100, 100, 100], [100, 100, 100, 100]]), 2, 100,
                               np.array([[0, 0], [0, 0]]))])
    def test_crop(self, tested_matrix, resolution, threshold, expected_matrix):
        tested_generated_element = GeneratedElement(tested_matrix, resolution)
        tested_generated_element.crop(threshold, True)
        cropped_matrix = tested_generated_element.matrix
        assert np.array_equal(cropped_matrix, expected_matrix)

    @pytest.mark.parametrize(
        '''gen1, gen2, expected_matrix, expected_resolution, left_upper_corner, in_resolution,
        replace, bigger_values_only''',
        [(GeneratedElement(np.array([[1, 2, 3, 4], [5, 6, 7, 8]]), 1),
          GeneratedElement(np.array([[-8, -7, -6, -5], [-4, -3, -2, -1]]), 1),
          np.array([[-7, -5, -3, -1], [1, 3, 5, 7]]), 1, (0, 0), True, False, False),
         (GeneratedElement(np.array([[1, 2, 3, 4], [5, 6, 7, 8]]), 1),
          GeneratedElement(np.array([[-7, -5], [-4, -2]]), 1),
          np.array([[-6, -3, 3, 4], [1, 4, 7, 8]]), 1, (0, 0), True, False, False),
         (GeneratedElement(np.array([[2, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -2, -2], [0, 0, 0, 0]]), 2),
          GeneratedElement(np.array([[-4, -1], [2, 5]]), 2),
          np.array([[2, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -6, -3], [0, 0, 2, 5]]), 2, (2, 2), True, False, False),
         (GeneratedElement(np.array([[2, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -2, -2], [0, 0, 0, 0]]), 1),
          GeneratedElement(np.array([[-4, -1], [2, 5]]), 1),
          np.array([[2, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -2, -2], [0, 0, 0, -4]]), 1, (3, 3), True, False, False),
         (GeneratedElement(np.array([[2, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -2, -2], [0, 0, 0, 0]]), 2),
          GeneratedElement(np.array([[-4, -1], [2, 5]]), 2),
          np.array([[7, 2, 2, 2], [0, 0, 0, 0], [-2, -2, -2, -2], [0, 0, 0, 0]]), 2, (-1, -1), True, False, False),
         (GeneratedElement(np.array([[9, 9, 9, 9], [-9, -9, -9, -9]]), 2),
          GeneratedElement(np.array([[1, 1], [-1, -1]]), 2),
          np.array([[9, 9, 10, 10], [-9, -9, -10, -10]]), 2, (0, 1), False, False, False),
         (GeneratedElement(np.array([[1, 1], [1, 1]]), 1),
          GeneratedElement(np.array([[2, 2], [2, 2]]), 1),
          np.array([[2, 2], [2, 2]]), 1, (0, 0), True, True, False),
         (GeneratedElement(([[1, 1, 1, 1], [1, 1, 1, 1]]), 1),
          GeneratedElement(np.array([[3, 3], [3, 3]]), 1),
          np.array([[1, 3, 3, 1], [1, 3, 3, 1]]), 1, (0, 1), True, False, True),
         (GeneratedElement(np.array([[-2, -3], [2, 3]]), 1),
          GeneratedElement(np.array([[0, 0], [0, 0]]), 1),
          np.array([[0, 0], [2, 3]]), 1, (0, 0), True, False, True)])
    def test_place(self,
                   gen1,
                   gen2,
                   expected_matrix,
                   expected_resolution,
                   left_upper_corner,
                   in_resolution,
                   replace,
                   bigger_values_only):
        gen1.place(gen2, left_upper_corner, in_resolution, replace, bigger_values_only, inplace=True)
        assert np.array_equal(gen1.matrix, expected_matrix)


@pytest.mark.parametrize('tested_matrix, expected_resolution, left_side, top_side, expected_matrix',
                         [(np.array([[2, 1, 3, 0], [-99, -8, -8.7, 999], [22, 0.0001, -1, 9]]), 3, True, True,
                           np.array([[0, 0, 2, 1, 3, 0], [0, 0, -99, -8, -8.7, 999], [0, 0, 22, 0.0001, -1, 9]])),
                          (np.array([[2, 7], [4, 14]]), 3, True, True, np.array([[0, 0, 0], [0, 2, 7], [0, 4, 14]])),
                          (np.array([[-9999, 88.444, -0.0001], [-9999, 88.444, -0.0001], [-9999, 88.444, -0.0001]]),
                           2, True, True,
                           np.array([[0, 0, 0, 0], [0, -9999, 88.444, -0.0001], [0, -9999, 88.444, -0.0001],
                                     [0, -9999, 88.444, -0.0001]])),
                          (np.array([[444444]]), 2, True, True, np.array([[0, 0], [0, 444444]])),
                          (np.array([[0]]), 1, True, True, np.array([[0]])),
                          (np.array([[-9999]]), 1, True, True, np.array([[-9999]])),
                          (np.array([[2, 1, 3, 0], [-99, -8, -8.7, 999], [22, 0.0001, -1, 9]]), 3, False, True,
                           np.array([[2, 1, 3, 0, 0, 0], [-99, -8, -8.7, 999, 0, 0], [22, 0.0001, -1, 9, 0, 0]])),
                          (np.array([[2, 1, 3, 0], [-99, -8, -8.7, 999], [22, 0.0001, -1, 9]]), 3, True, False,
                           np.array([[0, 0, 2, 1, 3, 0], [0, 0, -99, -8, -8.7, 999], [0, 0, 22, 0.0001, -1, 9]])),
                          (np.array([[2, 1], [3, 7]]), 3, True, False, np.array([[0, 2, 1], [0, 3, 7], [0, 0, 0]])),
                          (np.array([[2, 1], [3, 7]]), 3, False, False, np.array([[2, 1, 0], [3, 7, 0], [0, 0, 0]]))])
def test_redress_matrix(tested_matrix, expected_resolution, left_side, top_side, expected_matrix):
    redressed_matrix = redress_matrix(tested_matrix, expected_resolution, left_side, top_side)
    assert np.array_equal(expected_matrix, redressed_matrix)


@pytest.fixture(scope="module")
def directory():
    yield pathlib.Path().absolute() / 'saved'
    shutil.rmtree(pathlib.Path().absolute() / 'saved')


@pytest.mark.parametrize(
    "building, prefix, postfix, result", [
        (
                create_element('building', (5, 5), 1, (1, 1), height=10, width=5,
                               length=10).generate_element_from_model(), "", "", "-0-.npy"),
        (
                create_element('building', (5, 5), 1, (1, 1), height=10, width=5,
                               length=10).generate_element_from_model(), "", "", "-1-.npy"),
        (
                create_element('building', (5, 5), 1, (1, 1), height=10, width=5,
                               length=10).generate_element_from_model(), "building", "", "building-0-.npy"),
        (
                create_element('building', (5, 5), 1, (1, 1), height=10, width=5,
                               length=10).generate_element_from_model(), "building", "under_ground",
                "building-0-under_ground.npy"),
        (
                create_element('building', (5, 5), 1, (1, 1), height=10, width=5,
                               length=10).generate_element_from_model(), "", "under_ground", "-0-under_ground.npy"),
    ]
)
def test_save(prefix, postfix, result, building, directory):
    save([building], prefix=prefix, postfix=postfix)
    directory_files = [file.name for file in directory.iterdir()]
    assert result in directory_files


@pytest.mark.parametrize(
    "building, prefix_postfix", [
        (create_element('building', (5, 5), 1, (1, 1), height=10, width=5, length=10).generate_element_from_model(),
         'first'),
        (create_element('building', (5, 5), 1, (1, 1), height=10, width=5, length=10).generate_element_from_model(),
         'second'),
    ]
)
def test_save_load(building, prefix_postfix, directory):
    save([building], prefix_postfix)
    loaded_element = load(prefix_postfix)[0]
    assert np.array_equal(building.matrix, loaded_element.matrix)
    assert building.resolution == loaded_element.resolution
    assert building.size == loaded_element.size
