#!/usr/bin/python
import pytest
import numpy as np
from src.artificialmap.helpers import apply_resolution, detach_resolution, change_resolution, digitize_number, digitize


@pytest.mark.parametrize(
    "size, resolution, point, result", [
        ((10, 10), 5, False, (50, 50)),
        ((0.1, 0.1), 5, False, (1, 1)),
        ((0.5, 0.5), 5, False, (2, 2)),
        ((0.5, 0.5), 5, True, (2.5, 2.5))
    ]
)
def test_apply_resolution(size, resolution, point, result):
    assert result == apply_resolution(size, resolution, point)


@pytest.mark.parametrize(
    "size, resolution, point, int_constraint, result", [
        ((100, 100), 5, False, True, (20, 20)),
        ((0, 0), 5, False, True, (1, 1)),
        ((100, 100), 5, False, False, (20, 20)),
        ((0.5, 0.5), 5, True, True, (0.1, 0.1)),
        ((0.5, 0.5), 5, False, True, ValueError)
    ]
)
def test_detach_resolution(size, resolution, point, int_constraint, result):
    if result == ValueError:
        with pytest.raises(ValueError):
            detach_resolution(size, resolution, point, int_constraint)
    else:
        assert result == detach_resolution(size, resolution, point, int_constraint)


@pytest.mark.parametrize(
    "size, from_resolution, to_resolution, result", [
        ((10, 10), 2, 5, (25, 25))
    ]
)
def test_change_resolution(size, from_resolution, to_resolution, result):
    assert result == change_resolution(size, from_resolution, to_resolution)


@pytest.mark.parametrize(
        "given_matrix, resolution ,expected_matrix",
        [
            (np.array([1.0, 1.1, 1.25, 1.5, 1.75]), 1,  np.array([1., 2., 2., 2., 2.])),
            (np.array([1.0, 1.1, 1.25, 1.5, 1.75]), 2, np.array([1., 1.5, 1.5, 1.5, 2])),
            (np.array([1.0, 1.1, 1.25, 1.5, 1.75]), 4, np.array([1., 1.25, 1.25, 1.5, 1.75])),
            (np.array([1.0, 1.1, 1.25, 1.5, 1.75]), 5, np.array([1., 1.2, 1.4, 1.6, 1.8])),
            (np.array([1.0, 1.1, 1.25, 1.5, 1.75]), 10, np.array([1.,  1.1, 1.3, 1.5, 1.8])),

            (np.array([-1.0, -1.1, -1.25, -1.5, -1.75]), 1, np.array([-1., -2., -2., -2., -2.])),
            (np.array([-1.0, -1.1, -1.25, -1.5, -1.75]), 2, np.array([-1., -1.5, -1.5, -1.5, -2])),
            (np.array([-1.0, -1.1, -1.25, -1.5, -1.75]), 4, np.array([-1., -1.25, -1.25, -1.5, -1.75])),
            (np.array([-1.0, -1.1, -1.25, -1.5, -1.75]), 5, np.array([-1., -1.2, -1.4, -1.6, -1.8])),
            (np.array([-1.0, -1.1, -1.25, -1.5, -1.75]), 10, np.array([-1., -1.1, -1.3, -1.5, -1.8])),
        ]
    )
def test_digitize(given_matrix, resolution, expected_matrix):
    output_matrix = digitize(matrix=given_matrix, resolution=resolution)
    assert np.array_equal(output_matrix, expected_matrix)


@pytest.mark.parametrize(
        "given_number, resolution, expected_number",
        [
            (3.14, 1, 4),
            (3.14, 2, 3.5),
            (3.14, 4, 3.25),
            (3.14, 5, 3.2),
            (3.14, 10, 3.2),

            (-3.14, 1, -4.),
            (-3.14, 2, -3.5),
            (-3.14, 4, -3.25),
            (-3.14, 5, -3.2),
            (-3.14, 10, -3.2)
        ]
    )
def test_digitize_number(given_number, resolution, expected_number):
    output_number = digitize_number(num=given_number, resolution=resolution)
    assert np.array_equal(output_number, expected_number)
