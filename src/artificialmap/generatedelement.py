from __future__ import annotations
from pathlib import Path
import re
import numpy as np
from typing import Sequence, List
from artificialmap.helpers import detach_resolution, apply_resolution, digitize
from PIL import Image
from copy import copy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # do not delete


class GeneratedElement:
    """
    GeneratedElement defines operations for already generated element from model and binds features specific
    for element's matrix.
    """

    def __init__(self,
                 matrix: Sequence,
                 resolution: int,
                 digitize_matrix=True,
                 reflection_coefficient: float = 1,
                 stddev_surface_roughness: float = 0):
        """
        :param matrix: 2D matrix with values which represent height at point.
        :param resolution: determines the precise the matrix was generated with.
        :param digitize_matrix: when set on True values from matrix will be digitized to the given resolution. For
            example, when resolution will be set to 4, values of height will be put to bins like 0, 0.25, 0.5, ..., 2.75
            etc (in general, the step is '1 / resolution'.
        :param reflection_coefficient: reflection coefficient for electromagnetic wave. Its value should be in range
            <0, 1>.
        :param stddev_surface_roughness: standard deviation of element's surface roughness. It value should be greater
            than 0.
        """
        self.matrix_to_digitize = digitize_matrix
        self.matrix = digitize(matrix, resolution) if digitize_matrix else matrix
        self.resolution = resolution
        self.size = detach_resolution(self.matrix.shape, self.resolution)
        self.scaled_size = self.matrix.shape

        # radio
        self.reflection_coefficient = reflection_coefficient
        self.stddev_surface_roughness = stddev_surface_roughness

    def place(self,
              element: GeneratedElement,
              left_upper_corner: Sequence[int] = (0, 0),
              in_resolution: bool = True,
              replace: bool = False,
              bigger_values_only: bool = False,
              inplace: bool = False,
              digitize_result: bool = True) -> GeneratedElement:
        """
        Places matrix of a first element on matrix of a second element on corresponding position.

        :param element: element to place on.
        :param left_upper_corner: relative position on main matrix where other matrix will have LEFT UPPER CORNER
        :param in_resolution: indicates whether left upper corner refers to scaled matrix size
            (depends on the upsample parameter and actual resolutions of matrices) or to its original size
        :param replace: when set on True cells from main element's matrix will be replaced by cells from
            other element's matrix, when set on False values from both cells will be added (on corresponding positions)
        :param bigger_values_only: when set on True cells from main element will be REPLACED by cells from other element
            if values o other's element will be bigger. Parameter has influence when replace is set on False.
        :param inplace: when set on True an instance's matrix will be changed and returned.
        :param digitize_result: when set on True resulting GeneratedElement's matrix will be digitized by
            helpers.digitize function
        :return: GeneratedElement instance with combined matrices.
        """

        if self.resolution != element.resolution:
            raise ValueError("combined elements must have the same resolution")

        luc = apply_resolution(left_upper_corner, self.resolution, point=True) \
            if not in_resolution else left_upper_corner

        element_prev_shape = element.matrix.shape  # element's shape before adjusting
        element_ax_0 = _window(self.matrix.shape[0], element.matrix.shape[0], luc[0])
        element_ax_1 = _window(self.matrix.shape[1], element.matrix.shape[1], luc[1])
        element_piece = element.matrix[element_ax_0, element_ax_1]

        main_matrix = np.copy(self.matrix)
        if replace or bigger_values_only or element_piece.any():
            rev_luc_0 = luc[0] + element_prev_shape[0] - main_matrix.shape[0]
            rev_luc_1 = luc[1] + element_prev_shape[1] - main_matrix.shape[1]
            main_ax_0 = _window(element_prev_shape[0], main_matrix.shape[0], rev_luc_0)
            main_ax_1 = _window(element_prev_shape[1], main_matrix.shape[1], rev_luc_1)
            element_piece = np.flip(element_piece, (0, 1))
            main_matrix = np.flip(main_matrix, (0, 1))
            if replace:
                main_matrix[main_ax_0, main_ax_1] = element_piece
            elif bigger_values_only:
                main_matrix[main_ax_0, main_ax_1] = np.where(main_matrix[main_ax_0, main_ax_1] >= element_piece,
                                                             main_matrix[main_ax_0, main_ax_1],
                                                             element_piece)
            else:
                main_matrix[main_ax_0, main_ax_1] += element_piece
            main_matrix = np.flip(main_matrix, (0, 1))

        if inplace:
            self.matrix = main_matrix
            return self
        return GeneratedElement(main_matrix,
                                self.resolution,
                                digitize_result,
                                self.reflection_coefficient,
                                self.stddev_surface_roughness)

    def crop(self, threshold: float = 0, inplace: bool = False, **kwargs) -> GeneratedElement:
        """
        Crops matrix removing extreme rows/columns where all values are lesser than the threshold.
        Result matrix is redressed by generatedelement.redress_matrix functions with parameters left_side=True
        and top_side=True (by default).

        :param threshold: all extreme rows/columns with all values below will be cropped.
        :param inplace: when set on True an instance's matrix will be changed and returned.
        :return: GeneratedElement instance with cropped matrix.
        """

        below_threshold = self.matrix <= threshold
        top_ix = bottom_ix = left_ix = right_ix = None
        nothing_to_see = False

        if below_threshold.all():
            nothing_to_see = True
        elif below_threshold.any():
            top_ix = np.where((self.matrix <= threshold).all(1) == False)[0][0]
            bottom_ix = self.matrix.shape[0] - np.where((self.matrix[::-1] <= threshold).all(1) == False)[0][0]
            left_ix = np.where((self.matrix.T <= threshold).all(1) == False)[0][0]
            right_ix = self.matrix.shape[1] - np.where((self.matrix.T[::-1] <= threshold).all(1) == False)[0][0]

        cropped_matrix = np.array([[0]]) if nothing_to_see else copy(self.matrix[top_ix:bottom_ix, left_ix:right_ix])
        cropped_matrix = redress_matrix(cropped_matrix, self.resolution, **kwargs)
        if inplace:
            self.matrix = cropped_matrix
            return self
        return GeneratedElement(cropped_matrix,
                                self.resolution,
                                self.matrix_to_digitize,
                                self.reflection_coefficient,
                                self.stddev_surface_roughness)

    def rotate(self, angle: float, inplace: bool = False, **kwargs) -> GeneratedElement:
        """
        Rotates the matrix with a given angle. Result matrix is redressed by GeneratedElement.redress_matrix method with
        parameters left_side=True and top_side=True (by default).

        :param angle: counter clock-wise rotation of the element's matrix
        :param inplace: when set on True - changes element's matrix in place, when on False returns new GeneratedElement
            object with rotated matrix
        :return: rotated matrix
        """
        rotated_matrix = np.asarray(Image.fromarray(self.matrix).rotate(angle, expand=True))
        rotated_matrix = redress_matrix(rotated_matrix, self.resolution, **kwargs)
        if inplace:
            self.matrix = rotated_matrix
            return self
        return GeneratedElement(rotated_matrix,
                                self.resolution,
                                self.matrix_to_digitize,
                                self.reflection_coefficient,
                                self.stddev_surface_roughness)

    def show(self) -> None:
        """
        Shows element's matrix as colormap and its 3D representation.
        """
        fig = plt.figure(figsize=(8, 4))
        ax = fig.add_subplot(1, 2, 1)
        im = ax.imshow(self.matrix)
        fig.colorbar(im, ax=ax)
        ax = fig.add_subplot(1, 2, 2, projection='3d')
        x = np.arange(self.matrix.shape[1])
        y = np.arange(self.matrix.shape[0])
        x, y = np.meshgrid(x, y)
        ax.plot_surface(x, y, self.matrix, cmap='coolwarm')
        plt.show()


def _window(frame_size: int, span_size: int, span_start_pt: int) -> slice:
    if span_start_pt >= 0 and frame_size - span_start_pt > 0:
        span = slice(frame_size - span_start_pt)
    elif span_start_pt < 0 < (span_size + span_start_pt):
        span = slice(-(span_size + span_start_pt), abs(span_start_pt) + frame_size)
    else:
        span = slice(span_size, None)
    return span


def save(elements: Sequence[GeneratedElement], prefix: str = '', postfix: str = '') -> None:
    """
    Saves created matrix with resolution to .file in npy format.
    :param elements: list of the elements from the map.
    :param prefix: prefix to file name.
    :param postfix: prefix to file name.
    """
    directory_path = Path().absolute() / 'saved'
    if not Path(directory_path).exists():
        Path(directory_path).mkdir(parents=True, exist_ok=True)

    directory_files = [file.name for file in directory_path.iterdir()]

    pattern = re.compile(r'{}-\d+-{}'.format(prefix, postfix))
    directory_files_regex = [pattern.match(file).group() for file in directory_files if
                             pattern.match(file) is not None]
    new_element_id = int(max((re.findall(r'-(\d+)-', element_index) for element_index in directory_files_regex),
                             default=[-1])[0])

    for element in elements:
        to_save = np.array([np.nonzero(element.matrix), element.matrix[np.nonzero(element.matrix)],
                            len(element.matrix), len(element.matrix[0]), element.resolution])

        new_element_id += 1
        path_to_file = directory_path.as_posix() + '/{}-{}-{}'.format(prefix, new_element_id, postfix)
        np.save(path_to_file, to_save)


def load(regexp: str = '') -> List[GeneratedElement]:
    """
    Loads elements from directory, which names match a given regexp pattern.
    :param regexp: the phrase the search file contains.
    :return: list of GeneratedElement class instances.
    """
    list_of_elements = []
    directory_path = Path().absolute() / 'saved'
    directory_files = [file.name for file in directory_path.iterdir()]
    directory_files_regex = [file for file in directory_files if re.search(regexp, file) is not None]

    for element in directory_files_regex:
        file_path = Path(directory_path.as_posix() + '/{}'.format(element))
        file_data = np.load(file_path, allow_pickle=True)
        matrix = np.zeros((file_data[2], file_data[3]))
        matrix[file_data[0]] = file_data[1]

        list_of_elements.append(GeneratedElement(matrix, file_data[4]))

    return list_of_elements


def redress_matrix(matrix: np.ndarray,
                   expected_resolution: int,
                   left_side: bool = True,
                   top_side=True) -> np.ndarray:
    """
    Adjusts matrix size by adding zeros to preserve matrix having size incompatible with its resolution.

    :param matrix: input matrix.
    :param expected_resolution: resolution we want the matrix had.
    :param left_side: when set on True zeros will be added on the left side; right side otherwise.
    :param top_side: when set on True zeros will be added on the top side; bottom side otherwise.
    :return: redressed matrix.
    """
    redress_ax_0 = matrix.shape[0] % expected_resolution
    redress_ax_1 = matrix.shape[1] % expected_resolution
    if redress_ax_0:
        redress_ax_0 = expected_resolution - redress_ax_0
    if redress_ax_1:
        redress_ax_1 = expected_resolution - redress_ax_1

    fill_0 = np.zeros((redress_ax_0, matrix.shape[1]))
    if top_side:
        redressed_matrix = np.concatenate([fill_0, matrix], axis=0)
    else:
        redressed_matrix = np.concatenate([matrix, fill_0], axis=0)

    fill_1 = np.zeros((redressed_matrix.shape[0], redress_ax_1))
    if left_side:
        redressed_matrix = np.concatenate([fill_1, redressed_matrix], axis=1)
    else:
        redressed_matrix = np.concatenate([redressed_matrix, fill_1], axis=1)

    return redressed_matrix
