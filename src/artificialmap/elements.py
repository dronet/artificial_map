from __future__ import annotations
import numpy as np
from typing import Union
from artificialmap.generatedelement import GeneratedElement, Sequence, detach_resolution, copy
from artificialmap.model import Model, ModelController
import random


class EnvironmentElement:
    """EnvironmentElement is a base class for classes that represent specific environment elements.
        It creates element based on recipe from the given model.
    (function)."""

    def __init__(self,
                 size: Sequence[int],
                 resolution: int,
                 model: Union[Model, ModelController]):
        """
        :param size: maximal dimensions of the element
        :param resolution: describes how small single block of a land is, i.e. map with size 10x20 meters and
            resolution equal to 10 will be represented by a matrix with 100 rows and 200 columns.
        :param model: function of two variables, which maps positions of a matrix cell to specific value.
        """
        self.size = size
        self.model = model
        self.resolution = resolution

    def merge(self,
              other: EnvironmentElement,
              relative_position=Sequence[float],
              in_resolution: bool = True,
              replace: bool = False,
              bigger_values_only: bool = False,
              upsample: bool = False,
              inplace: bool = False) -> EnvironmentElement:
        """
        Merges two elements by treating their models as a one.

        :param other: element to merge
        :param relative_position: relative position on main element where the CENTER of the other element
            will be placed. (0, 0) is a left upper corner of the element we invoke this method on.
        :param in_resolution: indicates whether relative position refers to scaled (by resolution) element size
            or to its original form
        :param replace: when set on True cells from main element will be REPLACED by cells from other element;
            when set on False values from both cells will be ADDED (on corresponding positions)
        :param bigger_values_only: when set on True cells from main element will be REPLACED by cells from other element
            if values o other's element will be bigger. Parameter has influence when replace is set on False.
        :param upsample: when set on True returned element will have the biggest resolution (from both input elements),
            the lowest otherwise
        :param inplace: when set on True an instance's parameter will be changed in place
        :return: new EnvironmentElement object with merged parameters or reference to invoking object when inplace
            parameter is set on True
        """
        resolution = max([self.resolution, other.resolution]) if upsample else min([self.resolution, other.resolution])
        other_model = copy(other.model)
        if in_resolution:
            relative_position = detach_resolution(relative_position, self.resolution)  # not resolution variable!
        other_model.central_position = relative_position
        model_controller = ModelController(self.model, [other_model], replace, bigger_values_only)
        if inplace:
            self.resolution = resolution
            self.model = model_controller
            return self
        return EnvironmentElement(self.size, resolution, model_controller)

    def generate_element_from_model(self, matrix_only: bool = False) -> Union[GeneratedElement, np.ndarray]:
        """
        Generates element from the model given in constructor.

        :param matrix_only: when set on False the method returns GeneratedElement instance; matrix only otherwise.
        :return: GeneratedElement instance or raw matrix.
        """
        matrix = self.model.generate(self.size, self.resolution)
        return matrix if matrix_only else GeneratedElement(matrix, self.resolution)

    def randomize(self,
                  no_of_points: int,
                  interference_elements: Sequence[EnvironmentElement],
                  inplace: bool) -> EnvironmentElement:
        """
        Adds interference to an area of a EnvironmentElement's instance placing other EnvironmentElement's instances
            in random locations.

        :param no_of_points: number of points in which we want to randomize/interfere in map.
        :param interference_elements: list of elements that we add to our element as interferences.
        :param inplace: if set on True an existing element will be replaced, otherwise new changed instance will be
            created.
        :return: changed instance of EnvironmentElement or new instance of EnvironmentElement.
        """
        new_element = copy(self)
        for point in range(no_of_points):
            location = (random.randint(0, self.size[0]), random.randint(0, self.size[1]))
            interference_element = random.choice(interference_elements)
            new_element.merge(other=interference_element,
                              relative_position=location,
                              in_resolution=False,
                              inplace=True)
        if inplace:
            self.model = new_element.model
            self.resolution = new_element.resolution
            return self
        return new_element


def create_element(label: str,
                   size: Sequence[int],
                   resolution: int,
                   central_position: Sequence[float],
                   in_resolution: bool = True,
                   under_ground=False,
                   **kwargs) -> EnvironmentElement:
    """
    This function is an EnvironmentElement instances factory. Specific instance with distinct internal parameters
    is distinguished by label.
    :param label: to distinguish specific model.
    :param size: dimensions of the element.
    :param central_position: place where coordinates (0, 0) of the model have their beginning.
    :param resolution: describes how small single block of a land is, i.e. map with size 10x20 meters and
        resolution equal to 10 will be represented by a matrix with 100 rows and 200 columns.
    :param in_resolution: indicates whether central position refers to scaled (by resolution) element
        size or to its original form.
    :param under_ground: if set on True negative values are being taken into account.
    :param kwargs: parameters specific to every element.
    :return: EnvironmentElement instance with specific parameters.
    """

    def rounded_hill_(i, j):
        return -(kwargs['height'] / (kwargs['radius'] ** 2)) * (np.power(i, 2) + np.power(j, 2)) + kwargs['radius']

    def building_(i, j):
        building = kwargs['height'] * np.ones_like(i)
        building = np.where(abs(i) <= kwargs['width']/2, building, 0)
        building = np.where(abs(j) <= kwargs['length']/2, building, 0)
        return building

    def elliptic_hill_(i, j):
        j_coeff = np.power(j, 2) / (kwargs['major_radius'] ** 2)
        i_coeff = np.power(i, 2) / (kwargs['minor_radius'] ** 2)
        return kwargs['height'] - np.sqrt((j_coeff + i_coeff) * kwargs['height'] ** 2)

    def road_element_(i, j):
        road_element = kwargs['road_height'] * np.ones_like(i)
        road_element = np.where(abs(i) <= kwargs['width'] / 2, road_element, 0)
        road_element = np.where(abs(j) <= kwargs['length'] / 2, road_element, 0)
        return road_element

    elements_models = {
        'rounded_hill': rounded_hill_,
        'building': building_,
        'elliptic_hill': elliptic_hill_,
        'road_element': road_element_
    }
    if in_resolution:
        central_position = detach_resolution(central_position, resolution, int_constraint=False)
    return EnvironmentElement(size, resolution, Model(elements_models[label], central_position, under_ground))
