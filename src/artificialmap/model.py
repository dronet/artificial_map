from __future__ import annotations
from typing import Callable, Sequence, Union
import numpy as np
from artificialmap.helpers import apply_resolution


class Model:
    """
    Model describes how some specific element should looks.
    """
    def __init__(self,
                 model: Callable[[float, float], float],
                 central_position: Sequence[float],
                 under_ground: bool = False):
        """
        :param model: two-variable function.
        :param central_position: shift of the CENTER ( [0,0] coordinates ) of the model.
        :param under_ground: when set on False values lesser than 0 will be ignored (treated as 0).
        """
        self.model = model
        self.central_position = central_position
        self.under_ground = under_ground

    def resolve(self, i: float, j: float) -> float:
        value = self.model(i - self.central_position[0], j - self.central_position[1])
        return value if self.under_ground or not self.under_ground and value >= 0 else 0

    def generate(self, size: Sequence[int], resolution: int) -> np.ndarray:
        """
        Generates element via given size, resolution and model.

        :param size: size of an output element
        :param resolution: resolution of an output element
        :return: generated element
        """
        scaled_size = apply_resolution(size, resolution)
        x = np.linspace(0, size[0]-1, scaled_size[0])
        y = np.linspace(0, size[1]-1, scaled_size[1])
        xv, yv = np.meshgrid(x, y)
        xv -= self.central_position[0]
        yv -= self.central_position[1]

        terrain = self.model(xv, yv)
        if not self.under_ground:
            terrain = np.where(terrain >= 0, terrain, 0)
        return terrain


class ModelController:
    """
    ModelController can be container for more than one model, giving result too.
    """
    def __init__(self,
                 main_model: Union[Model, ModelController],
                 additional_models: Sequence[Union[Model, ModelController]] = None,
                 replace=False,
                 bigger_values_only=True):
        self.main_model = main_model
        self.additional_models = [] if additional_models is None else additional_models
        self.replace = replace
        self.bigger_values_only = bigger_values_only

    def resolve(self, i, j):
        value = self.main_model.resolve(i, j)
        for model in self.additional_models:
            additional_value = model.resolve(i, j)
            if self.replace and additional_value != 0:
                value = additional_value
            elif self.bigger_values_only:
                if additional_value > value:
                    value = additional_value
            else:
                value += additional_value
        return value

    def generate(self, size: Sequence[int], resolution: int) -> np.ndarray:
        """
        Generates element via given size, resolution and model.

        :param size: size of an output element
        :param resolution: resolution of an output element
        :return: generated element
        """
        main_model_gen = self.main_model.generate(size, resolution)
        other_models_gen = [other_model.generate(size, resolution) for other_model in self.additional_models]

        final_model_gen = np.copy(main_model_gen)

        for other_model in other_models_gen:
            if self.replace:
                final_model_gen = np.where(other_model != 0, other_model, final_model_gen)
            elif self.bigger_values_only:
                final_model_gen = np.where(other_model > final_model_gen, other_model, final_model_gen)
            else:
                final_model_gen += other_model

        return final_model_gen
