import numpy as np
from typing import Sequence


def apply_resolution(size: Sequence, resolution: int, point: bool = False) -> tuple:
    """
    Sets element's size / point with respect to resolution. If any dimension would be lesser than 1 it is set to 1.

    :param size: initial value.
    :param resolution: number which indicates how accurate the map/element should be.
    :param point: when Set on True indicates to apply resolution on point (not size).
    :return: transformed size / point with respect to resolution.
    """
    i = size[0] * resolution
    j = size[1] * resolution
    if not point:
        i = int(i)
        i = 1 if i < 1 else i
        j = int(j)
        j = 1 if j < 1 else j
    return i, j


def detach_resolution(size: Sequence, resolution: int, point: bool = False, int_constraint: bool = True) -> tuple:
    """
    Restores element's size / point to the initial values.

    :param size: current size.
    :param resolution: current resolution.
    :param point: when set on True indicates to detach resolution on point (not size).
    :param int_constraint: when set on True the integer constraint will be preserved during detaching resolution.
    :return: restored size / point.
    """
    i = size[0]/resolution
    j = size[1]/resolution
    if not point:
        if int_constraint:
            i_int = int(i)
            j_int = int(j)
            if i_int != i or j_int != j:
                raise ValueError(f'cannot detach resolution {resolution} from {size} size, result would not be integer')
            else:
                i = i_int
                j = j_int
            i = 1 if i < 1 else i
            j = 1 if j < 1 else j
    return i, j


def change_resolution(size: Sequence,
                      from_resolution: int,
                      to_resolution: int,
                      point: bool = False,
                      int_constraint: bool = True) -> tuple:
    """
    Changes element's size / point.

    :param size: current size.
    :param from_resolution: current resolution.
    :param to_resolution: next resolution.
    :param point: when set on True indicates to change resolution of a point (not size).
    :param int_constraint: when set on True the integer constraint will be preserved during detaching resolution.
    :return: changed size / point.
    """
    detached = detach_resolution(size, from_resolution, point, int_constraint)
    return apply_resolution(detached, to_resolution, point)


def digitize(matrix: Sequence, resolution: int) -> np.ndarray:
    """
    Digitizes values (height points) to fulfill a given resolution.
    :param matrix: any sequence of numbers, it can be 1D.
    :param resolution: it determines a size of single bin, to which all values will be put. For example, for resolution
        2 bins will be like 0, 0.5, 1.0, 1.5, ..., and for resolution 3 will be 0, 0.333, 0.667, 1.0, 1.333, ... etc.
    :return: matrix with digitized values. Output values in matrix have 3 decimals after comma.
    """
    x = np.array(matrix, dtype=np.float64)
    neg_args = x < 0
    x = abs(x)
    step = 1 / resolution
    matrix_min = np.min(x)
    bins = np.arange(matrix_min - (((matrix_min * 1e3) % (step * 1e3)) / 1e3), np.max(x) + step, step)
    digitized = bins[np.digitize(np.add(x, -1e-6), bins)]
    digitized[neg_args] = -digitized[neg_args]
    return np.around(digitized, 3)


def digitize_number(num: float, resolution: int) -> float:
    """
    Digitizes single value to fulfill a given resolution.
    :param num: number to digitize.
    :param resolution: it determines a size of single bin, to which value will be put. For example, for resolution
        2 bins will be like 0, 0.5, 1.0, 1.5, ..., and for resolution 3 will be 0, 0.333, 0.667, 1.0, 1.333, ... etc.
    :return: digitized value.
    """
    num_up = num * 1e6
    step = 1e6/resolution
    rest_up = abs(num_up) % step
    rest = rest_up/1e6
    if 0 < rest < 1e-6:
        return float(np.around(num, 3))
    elif rest > 1e-6:
        equalize = 1/resolution-rest if num > 0 else rest-1/resolution
        return float(np.around((num + equalize), 3))
    return num
