from artificialmap.generatedelement import GeneratedElement, apply_resolution, np
from typing import List, Sequence
from copy import deepcopy


class ArtificialMap:
    """
    ArtificialMap contains a set of GeneratedElements.
    """
    def __init__(self, size: Sequence[int], resolution: int):
        """
        :param size: size of the whole map
        :param resolution: describes how small single block of a land is, i.e. map with size 10x20 meters and resolution
            equal to 10 will be represented by a matrix with 100 rows and 200 columns.
        """
        self.size = size
        self.resolution = resolution
        self.scaled_size = apply_resolution(size, resolution)
        self.elements_on_map: List[dict] = []
        self._elements_version = 0
        self._last_known_version = 0
        self._generated_matrix = GeneratedElement(np.zeros(apply_resolution(self.size, self.resolution)),
                                                  self.resolution)

    def add_to_map(self, element: GeneratedElement, left_upper_corner: Sequence[int], **attributes):
        """
        Records a deep copy of a GeneratedElement instance on a given position and constraints.

        :param element: GeneratedElement object to add.
        :param left_upper_corner: position of object's left_upper_corner on the ArtificialMap.
        :param attributes: keyword arguments for concatenate method.
        """
        self.elements_on_map.append({'element': deepcopy(element),
                                     'left_upper_corner': left_upper_corner,
                                     **attributes})
        self._elements_version += 1

    def get_map(self) -> GeneratedElement:
        """
        :return: a map as GeneratedElement object with all elements (already added) combined.
        """
        if self._last_known_version != self._elements_version:
            self._last_known_version = self._elements_version
            ready_map = GeneratedElement(np.zeros(apply_resolution(self.size, self.resolution)), self.resolution)
            for element in self.elements_on_map:
                ready_map = ready_map.place(**element, digitize_result=False)
            self._generated_matrix = GeneratedElement(ready_map.matrix, self.resolution, digitize_matrix=True)
            return self._generated_matrix
        else:
            return self._generated_matrix

    def elements_on_point(self,
                          point: Sequence[int],
                          in_resolution: bool = True,
                          threshold: float = None) -> List[dict]:
        """
        Returns all elements at a given point.

        :param point: coordinates on a map
        :param in_resolution: indicates whether left upper corner of added elements refers to scaled map size
            (by resolution) or to its original size
        :param threshold: the height of a element at the given point must be greater or equal to threshold if set
        :return: all elements at a given point which fulfill optional threshold condition
        """
        elements = []
        if not in_resolution:
            point = apply_resolution(point, self.resolution)
        for element in self.elements_on_map:
            luc = element['left_upper_corner'] if not in_resolution else \
                apply_resolution(element['left_upper_corner'], self.resolution)
            if (luc[0] <= point[0] <= luc[0] + element['element'].matrix.shape[0] and
                    luc[1] <= point[1] <= luc[1] + element['element'].matrix.shape[1]):
                if (threshold and element['element'].matrix[point[0]-luc[0], point[1]-luc[1]] >= threshold or
                        threshold is None):
                    elements.append(element)
        return elements

    def height_on_point(self, point: Sequence[int]) -> float:
        return self.get_map().matrix[point]
