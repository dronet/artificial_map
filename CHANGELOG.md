# 'master' branch changelog (new features or changed features)

## v. 0.1.0
1. Created new class `GeneratedElement` which defines operations for already generated element from model and bounds features specific
    for element's matrix. Previously defined functions: `concatenate` and `crop` have been moved to this class as they represents common logic.
2. The bodies of methods (`GeneratedElement` class): `concatenate` and `crop` have been refined.
3. Some variables have been renamed.
4. The `generate_terrain` function has been renamed to `generate` and moved to `EnvironmentElement` class.

## v. 0.1.1
1. `merge` method from the `EnvironmentElement` class now returns new EnironmentElement instance instead of generated matrix. If `inplace` parameter is set on `True`, an instance on which we invoke this method will be changed and returned.
2.  Default value of `replace` parameter of the `merge` method (EnvironmentElement class) has been changed to `False`.
2. `crop` and `concatenate` methods from the `GeneratedElement` class now returns new GeneratedElement instance instead of ndarray. If `inplace` parameter is set on True, an instance on which we invoke this method will be changed and returned.
4. Added **jupyter notebook** with examples.
5. Added `EllipticHill` class to the `elemenets` package.
6. `create_element_from_model` method of `EnvironmentElement` class was renamed to `generate_element_from_model` and now returns `GeneratedElemenet` instance or matrix only if `matrix_only` parameter is set on True.

## v. 0.1.2
1. Created class `ArtificialMap` responsible for holding different `GeneratedElement` class objects and performing operations on them.
2. `merge` method of `EnvironmentElement` class, `concatenate` method of `GeneratedElement` class and every constructor for classes from `elements.py` file have parameter **in_resolution** which indicates whether **relative_position/central_position/left_upper_corner** refers to resized (by resolution parameter) element/generated matrix.

## v. 0.1.3
1. Moved `EnvironmentElement` class definition to `elements.py` file.
2. `apply_resolution` and `detach_resolution` functions can differentiate between sizes and points now (important feature, because scaling of a point have different purpose).
3. `GeneratedElement` class got two new methods: `rotate` to rotating element's matrix and `resize_to_resolution` which resizes element's matrix to a given resolution.
4. `redress_matrix` function has been added to `helpers.py` file. It adjusts matrix size by adding zeros to preserve matrix having size incompatible with its resolution.

## v. 0.1.4
1. Moved `redress_matrix` function to `GeneratedElement` class (because its behaviour is quite exclusive).
2. Renamed `concatenate` method of `GeneratedElement` class to `place` and removed *crop_other_ground* parameter (and its influence on combined elements).

## v. 0.1.5
1. Changed children of EnvironmentElement class (Building, EllipticHill, etc.) to instance factory - `create_element` function.
2. Created more explanatory examples (jupyter notebooks in example dir).
3. While merging or combining two elements besides replacing elements or not, it is possible from know to indicate that we want to replace corresponding cells when the value of a second cell will be grater than a value from a first cell. It is done by bigger_values_only parameters, which can be applied both to `merge` method (EnvironmentElement class), `place` method (GeneratedElement class) and to `ModelController`'s constructor.

## v. 0.1.6
1. Two functions to `generatedelement` module have been added - `save` and `load`, which allows to save and load GeneratedElement instance's matrices.
2. Object given to `GeneratedElement` constructor (matrix parameter) is instantly converted to np.ndarray from now, so it is possible to provide this matrix as any sequence.
3. `redress_matrix` method (`GeneratedElement`) is a function now (in `generatedelement` module).
4. New function for digitization of matrix values (element's height) was added (`digitize`) to `helpers` module.
5. `place` method (`GeneratedElement`) can be done with use of elements which have <u>the same</u> resolutions. Mechanism of handling elements with different resolutions brought more troubles than benefits.
6. Removed unnecessary method `resize_to_resolution` (`GeneratedElement`) as elements with the same resolution can be combined from now.